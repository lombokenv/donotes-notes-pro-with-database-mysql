package items;

import Connection.Koneksi;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author retina
 */
public class All_Notes extends javax.swing.JFrame {
    private Connection konek;
    private Statement stm;
    
    private Login_Menu signin;
    private Add_Notes addNotes;
    
    int mousePx;
    int mousePy;
    public All_Notes() {
        initComponents();
        setLocationRelativeTo(null);
        combo_categories.removeAllItems();
        konek = new Koneksi().connect();
        String sql = "SELECT * FROM  categories";

            try {
              stm = konek.createStatement();
              ResultSet rs = stm.executeQuery(sql);
              while(rs.next()){
                  combo_categories.addItem(rs.getString("name_categories"));
              }
              konek.close();
             } catch (SQLException ex) {
                    Logger.getLogger(Add_Notes.class.getName()).log(Level.SEVERE, null, ex);
             }        
        
        getDataTabel();
        
        ///////////
        combo_notes.removeAllItems();
        String sql2 = "SELECT * FROM notes";
        try {
              Statement stm1 = konek.createStatement();
              ResultSet rs2 = stm.executeQuery(sql2);
              while(rs2.next()){
                  combo_notes.addItem(rs2.getString("title"));
              }
              konek.close();
             } catch (SQLException ex) {
                    Logger.getLogger(Add_Notes.class.getName()).log(Level.SEVERE, null, ex);
             }       
        //////////
    }
    public void getDataTabel(){
        
        DefaultTableModel tblMod = new DefaultTableModel();
        tblMod.addColumn("No");
        tblMod.addColumn("Title");
        tblMod.addColumn("Category");
        tblMod.addColumn("Content");
        tableResult.setModel(tblMod);
        TableColumnModel columnModel = tableResult.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(5);
        tableResult.setColumnModel(columnModel);
        columnModel.getColumn(1).setPreferredWidth(150);
        tableResult.setColumnModel(columnModel);
        columnModel.getColumn(2).setPreferredWidth(150);
        tableResult.setColumnModel(columnModel);
        columnModel.getColumn(3).setPreferredWidth(150);
        tableResult.setColumnModel(columnModel);
        
        tableResult.setColumnModel(columnModel);
        
        try {
            konek = new Koneksi().connect();
            String sql1 = "SELECT * FROM notes";
            stm = konek.createStatement();
            ResultSet rs = stm.executeQuery(sql1);
            int counter = 0;
            while(rs.next()){
                tblMod.addRow(new Object[]{
                    counter+=1,
                    rs.getString("title"),
                    rs.getString("category"),
                    rs.getString("isi_notes")
                
                });
                tableResult.setModel(tblMod);
            }
        } catch (SQLException ex) {
            Logger.getLogger(All_Notes.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        choise_categories = new javax.swing.JLabel();
        combo_categories = new javax.swing.JComboBox<>();
        choise_notes = new javax.swing.JLabel();
        combo_notes = new javax.swing.JComboBox<>();
        Profil = new javax.swing.JLabel();
        filterBtn = new javax.swing.JButton();
        email_view = new javax.swing.JLabel();
        nav_menu1 = new javax.swing.JLabel();
        searchbar = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableResult = new javax.swing.JTable();
        menu1 = new javax.swing.JLabel();
        deleteBtn = new javax.swing.JButton();
        viewNotes = new javax.swing.JButton();
        menu2 = new javax.swing.JLabel();
        UpdateBtn = new javax.swing.JButton();
        search_btn = new javax.swing.JLabel();
        username_view = new javax.swing.JLabel();
        navigasi_top_horizontal = new javax.swing.JLabel();
        bg_search = new javax.swing.JLabel();
        sidebar_left = new javax.swing.JLabel();
        close = new javax.swing.JLabel();
        maximize = new javax.swing.JLabel();
        minimize = new javax.swing.JLabel();
        TItle = new javax.swing.JLabel();
        window_dragged = new javax.swing.JLabel();
        header = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setMinimumSize(new java.awt.Dimension(800, 600));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 600));
        jPanel1.setLayout(null);

        choise_categories.setText("Pilih Kategori");
        jPanel1.add(choise_categories);
        choise_categories.setBounds(180, 130, 95, 15);

        combo_categories.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2" }));
        jPanel1.add(combo_categories);
        combo_categories.setBounds(180, 150, 150, 26);

        choise_notes.setText("Pilih Notes");
        jPanel1.add(choise_notes);
        choise_notes.setBounds(180, 490, 77, 15);

        combo_notes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2" }));
        jPanel1.add(combo_notes);
        combo_notes.setBounds(180, 515, 150, 26);

        Profil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/logo/profil_donotes.png"))); // NOI18N
        jPanel1.add(Profil);
        Profil.setBounds(47, 80, 80, 80);

        filterBtn.setText("Filter");
        filterBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                filterBtnMouseClicked(evt);
            }
        });
        jPanel1.add(filterBtn);
        filterBtn.setBounds(350, 150, 71, 25);

        email_view.setFont(new java.awt.Font("Ubuntu Light", 1, 12)); // NOI18N
        email_view.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        email_view.setText("romy@gmail.com");
        jPanel1.add(email_view);
        email_view.setBounds(9, 180, 150, 15);

        nav_menu1.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        nav_menu1.setText("   MENU");
        jPanel1.add(nav_menu1);
        nav_menu1.setBounds(0, 220, 170, 25);

        searchbar.setBackground(new java.awt.Color(71, 71, 71));
        searchbar.setFont(new java.awt.Font("Ubuntu", 3, 13)); // NOI18N
        searchbar.setForeground(new java.awt.Color(255, 255, 255));
        searchbar.setText("by category...");
        searchbar.setBorder(null);
        searchbar.setOpaque(false);
        searchbar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchbarActionPerformed(evt);
            }
        });
        jPanel1.add(searchbar);
        searchbar.setBounds(632, 60, 98, 30);

        tableResult.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableResult.setRowHeight(23);
        tableResult.setUpdateSelectionOnSort(false);
        jScrollPane2.setViewportView(tableResult);

        jPanel1.add(jScrollPane2);
        jScrollPane2.setBounds(180, 190, 610, 280);

        menu1.setBackground(new java.awt.Color(124, 121, 121));
        menu1.setFont(new java.awt.Font("Ubuntu", 1, 13)); // NOI18N
        menu1.setForeground(new java.awt.Color(255, 255, 255));
        menu1.setText("        Add Notes");
        menu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menu1MouseClicked(evt);
            }
        });
        jPanel1.add(menu1);
        menu1.setBounds(0, 246, 170, 30);

        deleteBtn.setText("Delete");
        deleteBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                deleteBtnMouseClicked(evt);
            }
        });
        jPanel1.add(deleteBtn);
        deleteBtn.setBounds(350, 515, 81, 25);

        viewNotes.setText("Back");
        viewNotes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                viewNotesMouseClicked(evt);
            }
        });
        jPanel1.add(viewNotes);
        viewNotes.setBounds(535, 515, 80, 25);

        menu2.setBackground(new java.awt.Color(124, 121, 121));
        menu2.setFont(new java.awt.Font("Ubuntu", 1, 13)); // NOI18N
        menu2.setForeground(new java.awt.Color(255, 255, 255));
        menu2.setText("        Notes");
        menu2.setOpaque(true);
        jPanel1.add(menu2);
        menu2.setBounds(0, 276, 170, 30);

        UpdateBtn.setText("View");
        UpdateBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                UpdateBtnMouseClicked(evt);
            }
        });
        jPanel1.add(UpdateBtn);
        UpdateBtn.setBounds(450, 515, 66, 25);

        search_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Item/search_btn.png"))); // NOI18N
        search_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                search_btnMouseClicked(evt);
            }
        });
        jPanel1.add(search_btn);
        search_btn.setBounds(730, 60, 35, 30);

        username_view.setFont(new java.awt.Font("Ubuntu", 1, 13)); // NOI18N
        username_view.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        username_view.setText("Username");
        jPanel1.add(username_view);
        username_view.setBounds(9, 165, 150, 16);

        navigasi_top_horizontal.setBackground(new java.awt.Color(204, 204, 204));
        navigasi_top_horizontal.setOpaque(true);
        jPanel1.add(navigasi_top_horizontal);
        navigasi_top_horizontal.setBounds(170, 119, 630, 481);

        bg_search.setIcon(new javax.swing.ImageIcon(getClass().getResource("/logo/search.png"))); // NOI18N
        bg_search.setText("jLabel7");
        bg_search.setOpaque(true);
        jPanel1.add(bg_search);
        bg_search.setBounds(620, 60, 140, 30);

        sidebar_left.setBackground(new java.awt.Color(153, 153, 153));
        sidebar_left.setOpaque(true);
        jPanel1.add(sidebar_left);
        sidebar_left.setBounds(0, 119, 170, 481);

        close.setIcon(new javax.swing.ImageIcon(getClass().getResource("/window/close_w.png"))); // NOI18N
        close.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                closeMouseClicked(evt);
            }
        });
        jPanel1.add(close);
        close.setBounds(12, 12, 16, 16);

        maximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/window/maximize_w.png"))); // NOI18N
        maximize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                maximizeMouseClicked(evt);
            }
        });
        jPanel1.add(maximize);
        maximize.setBounds(40, 12, 16, 16);

        minimize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/window/min_w.png"))); // NOI18N
        jPanel1.add(minimize);
        minimize.setBounds(68, 12, 16, 16);

        TItle.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        TItle.setForeground(new java.awt.Color(255, 255, 255));
        TItle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TItle.setText("#Dropnotes - All Notes");
        TItle.setToolTipText("");
        jPanel1.add(TItle);
        TItle.setBounds(367, 10, 156, 17);

        window_dragged.setBackground(new java.awt.Color(51, 51, 51));
        window_dragged.setOpaque(true);
        window_dragged.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                window_draggedMouseDragged(evt);
            }
        });
        window_dragged.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                window_draggedMousePressed(evt);
            }
        });
        jPanel1.add(window_dragged);
        window_dragged.setBounds(0, -5, 800, 40);

        header.setBackground(new java.awt.Color(25, 22, 22));
        header.setOpaque(true);
        jPanel1.add(header);
        header.setBounds(0, 35, 800, 83);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void window_draggedMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_window_draggedMouseDragged
        int kordinatX = evt.getXOnScreen();
        int kordinatY = evt.getYOnScreen();

        int getXKordinat = kordinatX-mousePx;
        int getYKordinat = kordinatY-mousePy;
        this.setLocation(getXKordinat, getYKordinat );
    }//GEN-LAST:event_window_draggedMouseDragged

    private void window_draggedMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_window_draggedMousePressed
        mousePx = evt.getX();
        mousePy = evt.getY();
    }//GEN-LAST:event_window_draggedMousePressed

    private void closeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_closeMouseClicked
        dispose();
    }//GEN-LAST:event_closeMouseClicked

    private void maximizeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_maximizeMouseClicked
        System.out.println("Yes im was mAximize");
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
    }//GEN-LAST:event_maximizeMouseClicked

    private void searchbarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchbarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchbarActionPerformed

    private void menu1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menu1MouseClicked
      addNotes = new Add_Notes();
      addNotes.setUsername(username_view.getText().toString());
      addNotes.setEmail(email_view.getText().toString());
      addNotes.setVisible(true);
      dispose();
    }//GEN-LAST:event_menu1MouseClicked

    private void deleteBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deleteBtnMouseClicked
       konek = new Koneksi().connect();
        try {
            stm = konek.createStatement();
            String comboNotes = (String) combo_notes.getSelectedItem();
            String sql3 = "DELETE FROM notes WHERE title='"+comboNotes+"'";
            stm.executeUpdate(sql3);
            getDataTabel();
        } catch (SQLException ex) {
            Logger.getLogger(All_Notes.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }//GEN-LAST:event_deleteBtnMouseClicked

    private void filterBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_filterBtnMouseClicked
        konek = new Koneksi().connect();
        String comboCategories = (String) combo_categories.getSelectedItem();
        if(comboCategories.equals("All")){

                try {
                stm = konek.createStatement();

                String sql5 = "SELECT DISTINCT notes.title,notes.category,notes.isi_notes FROM categories, notes WHERE categories.relasi_categories = notes.relasi_categories";
                ResultSet rs5 = stm.executeQuery(sql5);
                DefaultTableModel tblMod = new DefaultTableModel();
                int counter = 0;
                tblMod.addColumn("No");
                tblMod.addColumn("Title");
                tblMod.addColumn("Category");
                tblMod.addColumn("Content");
                tableResult.setModel(tblMod);
                TableColumnModel columnModel = tableResult.getColumnModel();
                columnModel.getColumn(0).setPreferredWidth(5);
                tableResult.setColumnModel(columnModel);
                columnModel.getColumn(1).setPreferredWidth(150);
                tableResult.setColumnModel(columnModel);
                columnModel.getColumn(2).setPreferredWidth(150);
                tableResult.setColumnModel(columnModel);
                columnModel.getColumn(3).setPreferredWidth(150);
                tableResult.setColumnModel(columnModel);

                tableResult.setColumnModel(columnModel);
                while(rs5.next()){
                    tblMod.addRow(new Object[]{
                        counter+=1,
                        rs5.getString("title"),
                        rs5.getString("category"),
                        rs5.getString("isi_notes")

                    });
                    tableResult.setModel(tblMod);
                }
            } catch (SQLException ex) {
                Logger.getLogger(All_Notes.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
                try {
                stm = konek.createStatement();

                String sql4 = "SELECT DISTINCT notes.title,notes.category,notes.isi_notes FROM categories JOIN notes WHERE notes.category LIKE '%"+comboCategories+"%'";
                ResultSet rs4 = stm.executeQuery(sql4);
                DefaultTableModel tblMod = new DefaultTableModel();
                int counter = 0;
                tblMod.addColumn("No");
                tblMod.addColumn("Title");
                tblMod.addColumn("Category");
                tblMod.addColumn("Content");
                tableResult.setModel(tblMod);
                TableColumnModel columnModel = tableResult.getColumnModel();
                columnModel.getColumn(0).setPreferredWidth(5);
                tableResult.setColumnModel(columnModel);
                columnModel.getColumn(1).setPreferredWidth(150);
                tableResult.setColumnModel(columnModel);
                columnModel.getColumn(2).setPreferredWidth(150);
                tableResult.setColumnModel(columnModel);
                columnModel.getColumn(3).setPreferredWidth(150);
                tableResult.setColumnModel(columnModel);

                tableResult.setColumnModel(columnModel);
                while(rs4.next()){
                    tblMod.addRow(new Object[]{
                        counter+=1,
                        rs4.getString("title"),
                        rs4.getString("category"),
                        rs4.getString("isi_notes")

                    });
                    tableResult.setModel(tblMod);
                }
            } catch (SQLException ex) {
                Logger.getLogger(All_Notes.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }//GEN-LAST:event_filterBtnMouseClicked

    private void search_btnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_search_btnMouseClicked
        konek = new Koneksi().connect();
        String byCategory = searchbar.getText().toString();
        if(byCategory.equals("All")){

                try {
                stm = konek.createStatement();

                String sql5 = "SELECT DISTINCT notes.title,notes.category,notes.isi_notes FROM categories, notes WHERE categories.relasi_categories = notes.relasi_categories";
                ResultSet rs5 = stm.executeQuery(sql5);
                DefaultTableModel tblMod = new DefaultTableModel();
                int counter = 0;
                tblMod.addColumn("No");
                tblMod.addColumn("Title");
                tblMod.addColumn("Category");
                tblMod.addColumn("Content");
                tableResult.setModel(tblMod);
                TableColumnModel columnModel = tableResult.getColumnModel();
                columnModel.getColumn(0).setPreferredWidth(5);
                tableResult.setColumnModel(columnModel);
                columnModel.getColumn(1).setPreferredWidth(150);
                tableResult.setColumnModel(columnModel);
                columnModel.getColumn(2).setPreferredWidth(150);
                tableResult.setColumnModel(columnModel);
                columnModel.getColumn(3).setPreferredWidth(150);
                tableResult.setColumnModel(columnModel);

                tableResult.setColumnModel(columnModel);
                while(rs5.next()){
                    tblMod.addRow(new Object[]{
                        counter+=1,
                        rs5.getString("title"),
                        rs5.getString("category"),
                        rs5.getString("isi_notes")

                    });
                    tableResult.setModel(tblMod);
                }
            } catch (SQLException ex) {
                Logger.getLogger(All_Notes.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
                try {
                stm = konek.createStatement();

                String sql4 = "SELECT DISTINCT notes.title,notes.category,notes.isi_notes FROM categories JOIN notes WHERE notes.category LIKE '%"+byCategory+"%'";
                ResultSet rs4 = stm.executeQuery(sql4);
                DefaultTableModel tblMod = new DefaultTableModel();
                int counter = 0;
                tblMod.addColumn("No");
                tblMod.addColumn("Title");
                tblMod.addColumn("Category");
                tblMod.addColumn("Content");
                tableResult.setModel(tblMod);
                TableColumnModel columnModel = tableResult.getColumnModel();
                columnModel.getColumn(0).setPreferredWidth(5);
                tableResult.setColumnModel(columnModel);
                columnModel.getColumn(1).setPreferredWidth(150);
                tableResult.setColumnModel(columnModel);
                columnModel.getColumn(2).setPreferredWidth(150);
                tableResult.setColumnModel(columnModel);
                columnModel.getColumn(3).setPreferredWidth(150);
                tableResult.setColumnModel(columnModel);

                tableResult.setColumnModel(columnModel);
                while(rs4.next()){
                    tblMod.addRow(new Object[]{
                        counter+=1,
                        rs4.getString("title"),
                        rs4.getString("category"),
                        rs4.getString("isi_notes")

                    });
                    tableResult.setModel(tblMod);
                }
            } catch (SQLException ex) {
                Logger.getLogger(All_Notes.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_search_btnMouseClicked

    private void UpdateBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_UpdateBtnMouseClicked

        Update update = new Update();
        update.setUsername(username_view.getText());
        String comboNotes = (String) combo_notes.getSelectedItem();
        konek = new Koneksi().connect();
        try {
            stm = konek.createStatement();
            String sql6 = "SELECT isi_notes FROM `notes` WHERE title = '"+comboNotes+"'";
            ResultSet rs6 = stm.executeQuery(sql6);
            if(rs6.next()){
                update.setIsiNotes(rs6.getString("isi_notes"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(All_Notes.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        update.setTitle(comboNotes);
        update.setVisible(true);
        dispose();
    }//GEN-LAST:event_UpdateBtnMouseClicked

    private void viewNotesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_viewNotesMouseClicked
       Add_Notes addNotes = new Add_Notes();
       addNotes.setVisible(true);
       dispose();
    }//GEN-LAST:event_viewNotesMouseClicked

    public void setUsername(String username){
        username_view.setText(username);
    }
    public void setEmail(String email){
        email_view.setText(email);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Profil;
    private javax.swing.JLabel TItle;
    private javax.swing.JButton UpdateBtn;
    private javax.swing.JLabel bg_search;
    private javax.swing.JLabel choise_categories;
    private javax.swing.JLabel choise_notes;
    private javax.swing.JLabel close;
    private javax.swing.JComboBox<String> combo_categories;
    private javax.swing.JComboBox<String> combo_notes;
    private javax.swing.JButton deleteBtn;
    private javax.swing.JLabel email_view;
    private javax.swing.JButton filterBtn;
    private javax.swing.JLabel header;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel maximize;
    private javax.swing.JLabel menu1;
    private javax.swing.JLabel menu2;
    private javax.swing.JLabel minimize;
    private javax.swing.JLabel nav_menu1;
    private javax.swing.JLabel navigasi_top_horizontal;
    private javax.swing.JLabel search_btn;
    private javax.swing.JTextField searchbar;
    private javax.swing.JLabel sidebar_left;
    private javax.swing.JTable tableResult;
    private javax.swing.JLabel username_view;
    private javax.swing.JButton viewNotes;
    private javax.swing.JLabel window_dragged;
    // End of variables declaration//GEN-END:variables
}
