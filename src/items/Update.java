package items;

import Connection.Koneksi;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import items.All_Notes;
import javax.swing.JOptionPane;

public class Update extends javax.swing.JFrame {
    
    private Connection konek;
    private Statement stm;
    private Login_Menu signin;
    private All_Notes allNotes;
    private String titleChange;
    int mousePx;
    int mousePy;
    private int increments;
    
    public Update() {
        initComponents();
        setLocationRelativeTo(null);
        increments = 0;    
        
        combo_categories.removeAllItems();
        konek = new Koneksi().connect();
        String sql = "SELECT * FROM  categories";
            try {
              stm = konek.createStatement();
              ResultSet rs = stm.executeQuery(sql);
              while(rs.next()){
                  combo_categories.addItem(rs.getString("name_categories"));
              }
              konek.close();
             } catch (SQLException ex) {
                    Logger.getLogger(Update.class.getName()).log(Level.SEVERE, null, ex);
             }
        
        
    }
    
    public void setTitle(String Title){
        inputTitle.setText(Title);
        this.titleChange = Title;
    }
    public void setIsiNotes(String isi){
        isi_notes.setText(isi);
    }
   
    

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        choise_categories = new javax.swing.JLabel();
        title_label = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        isi_notes = new javax.swing.JTextArea();
        inputTitle = new javax.swing.JTextField();
        combo_categories = new javax.swing.JComboBox<>();
        Profil = new javax.swing.JLabel();
        email_view = new javax.swing.JLabel();
        nav_menu1 = new javax.swing.JLabel();
        updateBtn = new javax.swing.JButton();
        menu1 = new javax.swing.JLabel();
        menu2 = new javax.swing.JLabel();
        search_btn = new javax.swing.JLabel();
        username_view = new javax.swing.JLabel();
        backBtn = new javax.swing.JButton();
        navigasi_top_horizontal = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        bg_search = new javax.swing.JLabel();
        sidebar_left = new javax.swing.JLabel();
        close = new javax.swing.JLabel();
        maximize = new javax.swing.JLabel();
        minimize = new javax.swing.JLabel();
        TItle = new javax.swing.JLabel();
        window_dragged = new javax.swing.JLabel();
        header = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setMinimumSize(new java.awt.Dimension(800, 600));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 600));
        jPanel1.setLayout(null);

        choise_categories.setText("Pilih Kategori");
        jPanel1.add(choise_categories);
        choise_categories.setBounds(180, 170, 95, 15);

        title_label.setText("Title");
        jPanel1.add(title_label);
        title_label.setBounds(180, 135, 31, 15);

        isi_notes.setColumns(20);
        isi_notes.setRows(5);
        jScrollPane1.setViewportView(isi_notes);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(180, 230, 610, 270);
        jPanel1.add(inputTitle);
        inputTitle.setBounds(220, 130, 550, 30);

        combo_categories.setMaximumRowCount(20);
        combo_categories.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2" }));
        combo_categories.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                combo_categoriesMouseClicked(evt);
            }
        });
        jPanel1.add(combo_categories);
        combo_categories.setBounds(180, 190, 150, 26);

        Profil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/logo/profil_donotes.png"))); // NOI18N
        jPanel1.add(Profil);
        Profil.setBounds(47, 80, 80, 80);

        email_view.setFont(new java.awt.Font("Ubuntu Light", 1, 12)); // NOI18N
        email_view.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        email_view.setText("romy@gmail.com");
        jPanel1.add(email_view);
        email_view.setBounds(9, 180, 150, 15);

        nav_menu1.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        nav_menu1.setText("   MENU");
        jPanel1.add(nav_menu1);
        nav_menu1.setBounds(0, 220, 170, 25);

        updateBtn.setText("Update");
        updateBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                updateBtnMouseClicked(evt);
            }
        });
        jPanel1.add(updateBtn);
        updateBtn.setBounds(675, 520, 90, 40);

        menu1.setBackground(new java.awt.Color(124, 121, 121));
        menu1.setFont(new java.awt.Font("Ubuntu", 1, 13)); // NOI18N
        menu1.setForeground(new java.awt.Color(255, 255, 255));
        menu1.setText("        Add Notes");
        menu1.setOpaque(true);
        menu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menu1MouseClicked(evt);
            }
        });
        jPanel1.add(menu1);
        menu1.setBounds(0, 246, 170, 30);

        menu2.setBackground(new java.awt.Color(124, 121, 121));
        menu2.setFont(new java.awt.Font("Ubuntu", 1, 13)); // NOI18N
        menu2.setForeground(new java.awt.Color(255, 255, 255));
        menu2.setText("        Notes");
        menu2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menu2MouseClicked(evt);
            }
        });
        jPanel1.add(menu2);
        menu2.setBounds(0, 276, 170, 30);

        search_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Item/search_btn.png"))); // NOI18N
        jPanel1.add(search_btn);
        search_btn.setBounds(730, 60, 35, 30);

        username_view.setFont(new java.awt.Font("Ubuntu", 1, 13)); // NOI18N
        username_view.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        username_view.setText("Username");
        jPanel1.add(username_view);
        username_view.setBounds(9, 165, 150, 16);

        backBtn.setText("Back");
        backBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                backBtnMouseClicked(evt);
            }
        });
        jPanel1.add(backBtn);
        backBtn.setBounds(575, 520, 90, 40);

        navigasi_top_horizontal.setBackground(new java.awt.Color(204, 204, 204));
        navigasi_top_horizontal.setOpaque(true);
        jPanel1.add(navigasi_top_horizontal);
        navigasi_top_horizontal.setBounds(170, 119, 630, 481);

        jTextField1.setBackground(new java.awt.Color(71, 71, 71));
        jTextField1.setFont(new java.awt.Font("Ubuntu", 3, 13)); // NOI18N
        jTextField1.setForeground(new java.awt.Color(255, 255, 255));
        jTextField1.setText("by category...");
        jTextField1.setBorder(null);
        jTextField1.setOpaque(false);
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        jPanel1.add(jTextField1);
        jTextField1.setBounds(632, 60, 98, 30);

        bg_search.setIcon(new javax.swing.ImageIcon(getClass().getResource("/logo/search.png"))); // NOI18N
        bg_search.setText("jLabel7");
        bg_search.setOpaque(true);
        jPanel1.add(bg_search);
        bg_search.setBounds(620, 60, 140, 30);

        sidebar_left.setBackground(new java.awt.Color(153, 153, 153));
        sidebar_left.setOpaque(true);
        jPanel1.add(sidebar_left);
        sidebar_left.setBounds(0, 119, 170, 481);

        close.setIcon(new javax.swing.ImageIcon(getClass().getResource("/window/close_w.png"))); // NOI18N
        close.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                closeMouseClicked(evt);
            }
        });
        jPanel1.add(close);
        close.setBounds(12, 12, 16, 16);

        maximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/window/maximize_w.png"))); // NOI18N
        maximize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                maximizeMouseClicked(evt);
            }
        });
        jPanel1.add(maximize);
        maximize.setBounds(40, 12, 16, 16);

        minimize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/window/min_w.png"))); // NOI18N
        jPanel1.add(minimize);
        minimize.setBounds(68, 12, 16, 16);

        TItle.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        TItle.setForeground(new java.awt.Color(255, 255, 255));
        TItle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TItle.setText("#Dropnotes - Add Notes");
        TItle.setToolTipText("");
        jPanel1.add(TItle);
        TItle.setBounds(367, 10, 164, 17);

        window_dragged.setBackground(new java.awt.Color(51, 51, 51));
        window_dragged.setOpaque(true);
        window_dragged.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                window_draggedMouseDragged(evt);
            }
        });
        window_dragged.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                window_draggedMousePressed(evt);
            }
        });
        jPanel1.add(window_dragged);
        window_dragged.setBounds(0, -5, 800, 40);

        header.setBackground(new java.awt.Color(25, 22, 22));
        header.setOpaque(true);
        jPanel1.add(header);
        header.setBounds(0, 35, 800, 83);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void window_draggedMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_window_draggedMouseDragged
        int kordinatX = evt.getXOnScreen();
        int kordinatY = evt.getYOnScreen();

        int getXKordinat = kordinatX-mousePx;
        int getYKordinat = kordinatY-mousePy;
        this.setLocation(getXKordinat, getYKordinat );
    }//GEN-LAST:event_window_draggedMouseDragged

    private void window_draggedMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_window_draggedMousePressed
        mousePx = evt.getX();
        mousePy = evt.getY();
    }//GEN-LAST:event_window_draggedMousePressed

    private void closeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_closeMouseClicked
        dispose();
    }//GEN-LAST:event_closeMouseClicked

    private void maximizeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_maximizeMouseClicked
        System.out.println("Yes im was mAximize");
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
    }//GEN-LAST:event_maximizeMouseClicked

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void menu2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menu2MouseClicked
        allNotes = new All_Notes();
        allNotes.setUsername(username_view.getText().toString());
        allNotes.setEmail(email_view.getText().toString());
        allNotes.setVisible(true);
        dispose();
    }//GEN-LAST:event_menu2MouseClicked

    private void menu1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menu1MouseClicked

    }//GEN-LAST:event_menu1MouseClicked

    public String getTitle(){
        return this.titleChange;
    }
    private void updateBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_updateBtnMouseClicked
        konek = new Koneksi().connect();
        int relasi = 0;
            /////////////////
            String queryGetRelation = "SELECT relasi_user FROM user WHERE name = '"+username_view.getText()+"'";
            try {
                stm = konek.createStatement();
                ResultSet rs4 = stm.executeQuery(queryGetRelation);
                if(rs4.next()){
                    relasi = rs4.getInt("relasi_user");
                    
                }
            } catch (SQLException ex) {
                Logger.getLogger(Update.class.getName()).log(Level.SEVERE, null, ex);
            }
            /////////////////
            
        try {
            String comboCategories = (String) combo_categories.getSelectedItem();
            String title = inputTitle.getText().toString();
            String isiNotes = isi_notes.getText().toString();
            
           String titleNew = inputTitle.getText();
           String isiNotesNew = isi_notes.getText();
           String categoryNew = comboCategories;
           
//            String sqlUpdate = "UPDATE notes SET title ='"+inputTitle.getText()+"', isi_notes ='"+isi_notes.getText()+"', category = '"+comboCategories+"',relasi_categories='"+relasi+"', relasi_notes = '"+relasi+"' WHERE notes.title = '"+titleChange+"'";
            String sqlUpdate = "UPDATE notes SET title ='"+titleNew+"', isi_notes ='"+isiNotesNew+"', category = '"+categoryNew+"',relasi_categories='"+relasi+"', relasi_notes = '"+relasi+"' WHERE notes.title = '"+getTitle()+"'";
            
            Statement stm7 = konek.createStatement();
            stm7.executeUpdate(sqlUpdate);
            JOptionPane.showMessageDialog(this, "Sukses! Anda telah memperbarui note");
        } catch (SQLException ex) {
            Logger.getLogger(Update.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_updateBtnMouseClicked

    private void combo_categoriesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_combo_categoriesMouseClicked

       
       
    }//GEN-LAST:event_combo_categoriesMouseClicked

    private void backBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backBtnMouseClicked
       All_Notes allNotes = new All_Notes();
       allNotes.setVisible(true);
       dispose();
    }//GEN-LAST:event_backBtnMouseClicked
    public void setUsername(String username){
        username_view.setText(username);
    }
    public void setEmail(String email){
        email_view.setText(email);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Profil;
    private javax.swing.JLabel TItle;
    private javax.swing.JButton backBtn;
    private javax.swing.JLabel bg_search;
    private javax.swing.JLabel choise_categories;
    private javax.swing.JLabel close;
    private javax.swing.JComboBox<String> combo_categories;
    private javax.swing.JLabel email_view;
    private javax.swing.JLabel header;
    private javax.swing.JTextField inputTitle;
    private javax.swing.JTextArea isi_notes;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel maximize;
    private javax.swing.JLabel menu1;
    private javax.swing.JLabel menu2;
    private javax.swing.JLabel minimize;
    private javax.swing.JLabel nav_menu1;
    private javax.swing.JLabel navigasi_top_horizontal;
    private javax.swing.JLabel search_btn;
    private javax.swing.JLabel sidebar_left;
    private javax.swing.JLabel title_label;
    private javax.swing.JButton updateBtn;
    private javax.swing.JLabel username_view;
    private javax.swing.JLabel window_dragged;
    // End of variables declaration//GEN-END:variables
}
