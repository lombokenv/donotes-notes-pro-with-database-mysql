package items;

import Connection.Koneksi;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Login_Menu extends javax.swing.JFrame {
    private SignUp_Menu signup;
    private Add_Notes menuMain;
    private Connection konek;
    private Statement stm;
    private String Query;
    
    int mousePy;
    int mousePx;
    boolean max;
    public Login_Menu() {
   
        initComponents();
        setLocationRelativeTo(null);
   
    }

 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        logo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        window_dragged = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        passForm = new javax.swing.JPasswordField();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        userForm = new javax.swing.JTextField();
        try {
            signInBtn =(javax.swing.JLabel)java.beans.Beans.instantiate(getClass().getClassLoader(), "items.login_window_signInBtn");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        Layout_login = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        bg = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setMinimumSize(new java.awt.Dimension(800, 600));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 600));
        jPanel1.setLayout(null);

        logo.setFont(new java.awt.Font("DejaVu Sans Light", 1, 11)); // NOI18N
        logo.setText("Don't have account? Click here");
        logo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoMouseClicked(evt);
            }
        });
        jPanel1.add(logo);
        logo.setBounds(290, 340, 205, 14);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/window/close_w.png"))); // NOI18N
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel1);
        jLabel1.setBounds(12, 12, 16, 16);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/window/maximize_w.png"))); // NOI18N
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel2);
        jLabel2.setBounds(40, 12, 16, 16);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/window/min_w.png"))); // NOI18N
        jPanel1.add(jLabel3);
        jLabel3.setBounds(68, 12, 16, 16);

        jLabel4.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("#Dropnotes - Sign In");
        jLabel4.setToolTipText("");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(350, 10, 138, 17);

        window_dragged.setBackground(new java.awt.Color(51, 51, 51));
        window_dragged.setOpaque(true);
        window_dragged.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                window_draggedMouseDragged(evt);
            }
        });
        window_dragged.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                window_draggedMousePressed(evt);
            }
        });
        jPanel1.add(window_dragged);
        window_dragged.setBounds(0, -5, 800, 40);

        jLabel6.setFont(new java.awt.Font("DejaVu Sans Condensed", 1, 18)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("LOGIN");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(280, 140, 260, 40);

        jSeparator1.setBackground(new java.awt.Color(51, 51, 51));
        jSeparator1.setForeground(new java.awt.Color(51, 51, 51));
        jSeparator1.setToolTipText("");
        jSeparator1.setOpaque(true);
        jPanel1.add(jSeparator1);
        jSeparator1.setBounds(290, 250, 240, 3);

        jSeparator2.setBackground(new java.awt.Color(51, 51, 51));
        jSeparator2.setForeground(new java.awt.Color(51, 51, 51));
        jSeparator2.setOpaque(true);
        jPanel1.add(jSeparator2);
        jSeparator2.setBounds(290, 320, 240, 3);

        passForm.setBorder(null);
        passForm.setOpaque(false);
        passForm.setPreferredSize(new java.awt.Dimension(70, 19));
        passForm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passFormActionPerformed(evt);
            }
        });
        jPanel1.add(passForm);
        passForm.setBounds(290, 280, 240, 40);

        jLabel8.setText("Password");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(290, 260, 240, 15);

        jLabel7.setText("User ID");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(290, 190, 240, 15);

        userForm.setBorder(null);
        userForm.setOpaque(false);
        userForm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userFormActionPerformed(evt);
            }
        });
        jPanel1.add(userForm);
        userForm.setBounds(290, 210, 240, 40);

        signInBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                signInBtnMouseClicked(evt);
            }
        });
        jPanel1.add(signInBtn);
        signInBtn.setBounds(360, 390, 110, 40);

        Layout_login.setBackground(new java.awt.Color(255, 255, 255));
        Layout_login.setOpaque(true);
        jPanel1.add(Layout_login);
        Layout_login.setBounds(280, 140, 260, 320);

        jLabel5.setText("jLabel5");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(280, 10, 51, 15);

        bg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Item/home-banner.jpg"))); // NOI18N
        bg.setText("jLabel9");
        jPanel1.add(bg);
        bg.setBounds(0, 30, 800, 570);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void userFormActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userFormActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_userFormActionPerformed

    private void passFormActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passFormActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_passFormActionPerformed

    private void window_draggedMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_window_draggedMouseDragged
        int kordinatX = evt.getXOnScreen();
        int kordinatY = evt.getYOnScreen();
            
        int getXKordinat = kordinatX-mousePx;
        int getYKordinat = kordinatY-mousePy;
        this.setLocation(getXKordinat, getYKordinat );
    }//GEN-LAST:event_window_draggedMouseDragged

    private void signInBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_signInBtnMouseClicked
            String username = userForm.getText().toString();
            String password = passForm.getText().toString();
            
            konek = (Connection) new Koneksi().connect();
            Query = "SELECT * FROM user where name='"+username+"' or email='"+username+"' and password='"+password+"' ";
            if(username.equals("")){
                JOptionPane.showMessageDialog(this, "Anda belum menginputkan apapun");
            }else{
                try {
                    stm = konek.createStatement();
                    ResultSet rs = stm.executeQuery(Query);
                    if(rs.next()){
                        
                        String Query1 = "SELECT name FROM user where name='"+username+"'";
                        Statement stm1 = konek.createStatement();
                        ResultSet rs1 = stm1.executeQuery(Query1);
                        if(rs1.next()){
                            JOptionPane.showMessageDialog(this, "Sukses "+username+", anda telah terhubung.Tekan OK!");
                            
                            String Query2 = "SELECT email FROM user where name='"+username+"'";
                            Statement stm2 = konek.createStatement();
                            ResultSet rs2 = stm2.executeQuery(Query2);
                            
                            String email = null;
                            while(rs2.next()){
                                email = rs2.getString("email");
                            }
                            menuMain = new Add_Notes();
                            menuMain.setUsername(username);
                            menuMain.setEmail(email);
                            menuMain.setVisible(true);
                            dispose();
                        }
                    }else{
                        JOptionPane.showMessageDialog(this, "Username dan Password tidak tersedia.\nTekan OK!");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(Login_Menu.class.getName()).log(Level.SEVERE, null, ex);
                }
               
            }
            
//            menuMain = new Add_Notes();
//            menuMain.setVisible(true);
//            dispose();
    }//GEN-LAST:event_signInBtnMouseClicked

    private void window_draggedMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_window_draggedMousePressed
        mousePx = evt.getX();
        mousePy = evt.getY();
    }//GEN-LAST:event_window_draggedMousePressed

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        System.out.println("Yes im was clicked");
        dispose();
    }//GEN-LAST:event_jLabel1MouseClicked

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
       System.out.println("Yes im was mAximize");
           setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
   
    }//GEN-LAST:event_jLabel2MouseClicked

    private void logoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoMouseClicked
        signup = new SignUp_Menu();
        signup.setVisible(true);
        dispose();
    }//GEN-LAST:event_logoMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login_Menu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Layout_login;
    private javax.swing.JLabel bg;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel logo;
    private javax.swing.JPasswordField passForm;
    private javax.swing.JLabel signInBtn;
    private javax.swing.JTextField userForm;
    private javax.swing.JLabel window_dragged;
    // End of variables declaration//GEN-END:variables
}
