package items;

import Connection.Koneksi;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;


public class SignUp_Menu extends javax.swing.JFrame {
    private Connection konek;
    private ResultSet rs;
    private Statement stm;
    private String Query;
    private boolean check = false;
    
    private Login_Menu signin;
    int mousePx;
    int mousePy;
    private int autoIncrement;
    
    public SignUp_Menu() {
        initComponents();
        setLocationRelativeTo(null);
        autoIncrement = 0;
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        backBtn = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        window_dragged = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        emailAddress = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        fullName = new javax.swing.JTextField();
        signUpbtn = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        password = new javax.swing.JPasswordField();
        jLabel9 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        passwordConfirm = new javax.swing.JPasswordField();
        jLabel11 = new javax.swing.JLabel();
        genderFemale = new javax.swing.JRadioButton();
        genderMale1 = new javax.swing.JRadioButton();
        Layout_login = new javax.swing.JLabel();
        bg = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setMinimumSize(new java.awt.Dimension(800, 600));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 600));
        jPanel1.setLayout(null);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/window/close_w.png"))); // NOI18N
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel1);
        jLabel1.setBounds(12, 12, 16, 16);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/window/maximize_w.png"))); // NOI18N
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel2);
        jLabel2.setBounds(40, 12, 16, 16);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/window/min_w.png"))); // NOI18N
        jPanel1.add(jLabel3);
        jLabel3.setBounds(68, 12, 16, 16);

        backBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Item/left.png"))); // NOI18N
        backBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                backBtnMouseClicked(evt);
            }
        });
        jPanel1.add(backBtn);
        backBtn.setBounds(285, 105, 40, 40);

        jLabel4.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("#Dropnotes - Sign Up");
        jLabel4.setToolTipText("");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(350, 10, 144, 17);

        window_dragged.setBackground(new java.awt.Color(51, 51, 51));
        window_dragged.setOpaque(true);
        window_dragged.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                window_draggedMouseDragged(evt);
            }
        });
        window_dragged.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                window_draggedMousePressed(evt);
            }
        });
        jPanel1.add(window_dragged);
        window_dragged.setBounds(0, -5, 800, 40);

        jLabel6.setFont(new java.awt.Font("DejaVu Sans Condensed", 1, 18)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("SIGN UP");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(280, 100, 260, 40);

        jSeparator1.setBackground(new java.awt.Color(51, 51, 51));
        jSeparator1.setForeground(new java.awt.Color(51, 51, 51));
        jSeparator1.setToolTipText("");
        jSeparator1.setOpaque(true);
        jPanel1.add(jSeparator1);
        jSeparator1.setBounds(290, 210, 240, 3);

        jSeparator2.setBackground(new java.awt.Color(51, 51, 51));
        jSeparator2.setForeground(new java.awt.Color(51, 51, 51));
        jSeparator2.setOpaque(true);
        jPanel1.add(jSeparator2);
        jSeparator2.setBounds(290, 270, 240, 3);

        emailAddress.setBorder(null);
        emailAddress.setOpaque(false);
        emailAddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                emailAddressActionPerformed(evt);
            }
        });
        jPanel1.add(emailAddress);
        emailAddress.setBounds(290, 240, 240, 40);

        jLabel8.setText("Email");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(290, 220, 240, 15);

        jLabel7.setText("Full name");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(290, 160, 240, 15);

        fullName.setBorder(null);
        fullName.setOpaque(false);
        fullName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fullNameActionPerformed(evt);
            }
        });
        jPanel1.add(fullName);
        fullName.setBounds(290, 180, 240, 40);

        signUpbtn.setBackground(new java.awt.Color(204, 0, 204));
        signUpbtn.setForeground(new java.awt.Color(255, 255, 255));
        signUpbtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        signUpbtn.setText("Sign Up");
        signUpbtn.setOpaque(true);
        signUpbtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                signUpbtnMouseClicked(evt);
            }
        });
        jPanel1.add(signUpbtn);
        signUpbtn.setBounds(355, 450, 110, 40);

        jSeparator3.setBackground(new java.awt.Color(51, 51, 51));
        jSeparator3.setForeground(new java.awt.Color(51, 51, 51));
        jSeparator3.setOpaque(true);
        jPanel1.add(jSeparator3);
        jSeparator3.setBounds(290, 330, 240, 3);

        password.setBorder(null);
        password.setOpaque(false);
        password.setPreferredSize(new java.awt.Dimension(70, 19));
        password.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passwordActionPerformed(evt);
            }
        });
        jPanel1.add(password);
        password.setBounds(290, 300, 240, 40);

        jLabel9.setText("Password");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(290, 280, 240, 15);

        jSeparator4.setBackground(new java.awt.Color(51, 51, 51));
        jSeparator4.setForeground(new java.awt.Color(51, 51, 51));
        jSeparator4.setOpaque(true);
        jPanel1.add(jSeparator4);
        jSeparator4.setBounds(290, 390, 240, 3);

        passwordConfirm.setBorder(null);
        passwordConfirm.setOpaque(false);
        passwordConfirm.setPreferredSize(new java.awt.Dimension(70, 19));
        passwordConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passwordConfirmActionPerformed(evt);
            }
        });
        jPanel1.add(passwordConfirm);
        passwordConfirm.setBounds(290, 360, 240, 40);

        jLabel11.setText("Password Confirmed");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(290, 340, 240, 15);

        buttonGroup1.add(genderFemale);
        genderFemale.setText("Female");
        genderFemale.setActionCommand("Laki-laki");
        genderFemale.setOpaque(false);
        genderFemale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                genderFemaleActionPerformed(evt);
            }
        });
        jPanel1.add(genderFemale);
        genderFemale.setBounds(350, 410, 90, 23);

        buttonGroup1.add(genderMale1);
        genderMale1.setSelected(true);
        genderMale1.setText("Male");
        genderMale1.setOpaque(false);
        genderMale1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                genderMale1ActionPerformed(evt);
            }
        });
        jPanel1.add(genderMale1);
        genderMale1.setBounds(290, 410, 58, 23);

        Layout_login.setBackground(new java.awt.Color(255, 255, 255));
        Layout_login.setOpaque(true);
        jPanel1.add(Layout_login);
        Layout_login.setBounds(280, 100, 260, 420);

        bg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Item/home-banner.jpg"))); // NOI18N
        bg.setText("jLabel9");
        jPanel1.add(bg);
        bg.setBounds(0, 30, 800, 570);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void fullNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fullNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_fullNameActionPerformed

    private void passwordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passwordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_passwordActionPerformed

    private void passwordConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passwordConfirmActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_passwordConfirmActionPerformed

    private void emailAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_emailAddressActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_emailAddressActionPerformed

    private void genderFemaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_genderFemaleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_genderFemaleActionPerformed

    private void genderMale1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_genderMale1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_genderMale1ActionPerformed

    private void signUpbtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_signUpbtnMouseClicked
       String gender = null;
       String message = "";
       
       String username = fullName.getText().toString();
       konek = new Koneksi().connect();
       Query = "SELECT name FROM user WHERE name='"+username+"'";
   
       
       if(fullName.getText().equals("") && 
          emailAddress.getText().equals("") &&
          password.getText().equals("") &&
          passwordConfirm.getText().equals("")){
          JOptionPane.showMessageDialog(
                        null,
                        "Maaf,Anda Belum Menginputkan Apapun. Silahkan Lengkapi isian Form!");  
       }else{
               if(fullName.getText().equals("")){
                   message += " 'Username' ";
               }
               if(emailAddress.getText().equals("")){
                   message += " 'Email Address' ";         
               }
               if(password.getText().equals("")){
                   message += " 'Password' ";
               }
               if(passwordConfirm.getText().equals("")){
                   message += " 'Password Konfirmasi' ";
               }
               
               if(fullName.getText().equals("") || 
                    emailAddress.getText().equals("") ||
                    password.getText().equals("") ||
                    passwordConfirm.getText().equals("")){
                    JOptionPane.showMessageDialog(
                        null,
                        "Maaf,Anda Belum Menginputkan ("+
                         message
                         +"). \nSilahkan Lengkapi isian Form!");  
                 }else{
                   try {
                        stm = konek.createStatement();
                        rs = stm.executeQuery(Query);
                        if(rs.next()){
                            check = true;
    //                        JOptionPane.showMessageDialog(
    //                        null,
    //                        "Ketemu");  
                        }else{
                            check = false;
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(SignUp_Menu.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if(check){
                        JOptionPane.showMessageDialog(
                            null,
                            "Maaf, Username telah digunakan.\n Silahkan menginputkan username kembali");  
                    }else{
                        
                           if(password.getText().toLowerCase().equals(passwordConfirm.getText().toLowerCase())){
                                     Enumeration<AbstractButton> bg = buttonGroup1.getElements();
                                        while(bg.hasMoreElements()){
                                            JRadioButton jrd = (JRadioButton) bg.nextElement();
                                            if(jrd.isSelected())
                                                gender = jrd.getText();
                                        }
                                        
                                    String emailAdd = emailAddress.getText().toString();
                                    String passUser = password.getText().toString();
                                    autoIncrement +=1;
                                    
                                    String queryGetRelation = "SELECT relasi_user FROM user getLastRecord ORDER BY id_user DESC LIMIT 1";
                                    
                                    //////////////////////
                                    int mes = 0;
                                        try {
                                            stm = konek.createStatement();
                                            ResultSet rs3 = stm.executeQuery(queryGetRelation);
                                           if(rs3.next()){
                                               mes = rs3.getInt("relasi_user");
                                               autoIncrement+=mes;
                                           }
                                        } catch (SQLException ex) {
                                            ex.printStackTrace();
                                        }  
                                    /////////////////////    
                                    try {
                                      
                                        String Query1 = "INSERT INTO user VALUES(null,'"+username+"','"+emailAdd+"','"+passUser+"','"+gender+"','"+autoIncrement+"')";
                                        
                                        /////////////////////
                                        stm.executeUpdate(Query1);
                                        JOptionPane.showMessageDialog(
                                        null,
                                        "Data telah berhasil disimpan. Silahkan untuk melakukan Login");
                                        signin = new Login_Menu();
                                        signin.setVisible(true);
                                        dispose();
                                    } catch (SQLException ex) {
                                        Logger.getLogger(SignUp_Menu.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    
                                        
                             }else{
                                    JOptionPane.showMessageDialog(
                                                 null,
                                                 "Maaf, silahkan cek kembali password konfirmasi anda");  
                             }
                        
                        
                    }
               }
               
       }
      
       
//       if(password.getText().toLowerCase().equals(passwordConfirm.getText().toLowerCase())){
//           JOptionPane.showMessageDialog(
//                        null,
//                        "Sukses!");  
//       }else{
//           JOptionPane.showMessageDialog(
//                        null,
//                        "Gagal!");  
//       }
       
//        Enumeration<AbstractButton> bg = buttonGroup1.getElements();
//        while(bg.hasMoreElements()){
//            JRadioButton jrd = (JRadioButton) bg.nextElement();
//            if(jrd.isSelected())
//                gender = jrd.getText();
//        }
//        JOptionPane.showMessageDialog(
//                        null,
//                        gender);

//       signin = new Login_Menu();
//       signin.setVisible(true);
//       dispose();
    }//GEN-LAST:event_signUpbtnMouseClicked

    private void window_draggedMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_window_draggedMouseDragged
        int kordinatX = evt.getXOnScreen();
        int kordinatY = evt.getYOnScreen();

        int getXKordinat = kordinatX-mousePx;
        int getYKordinat = kordinatY-mousePy;
        this.setLocation(getXKordinat, getYKordinat );
    }//GEN-LAST:event_window_draggedMouseDragged

    private void window_draggedMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_window_draggedMousePressed
        mousePx = evt.getX();
        mousePy = evt.getY();
    }//GEN-LAST:event_window_draggedMousePressed

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        dispose();
    }//GEN-LAST:event_jLabel1MouseClicked

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        System.out.println("Yes im was mAximize");
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
    }//GEN-LAST:event_jLabel2MouseClicked

    private void backBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backBtnMouseClicked
        Login_Menu logMenu = new Login_Menu();
        logMenu.setVisible(true);
        dispose();
    }//GEN-LAST:event_backBtnMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SignUp_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SignUp_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SignUp_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SignUp_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SignUp_Menu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Layout_login;
    private javax.swing.JLabel backBtn;
    private javax.swing.JLabel bg;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JTextField emailAddress;
    private javax.swing.JTextField fullName;
    private javax.swing.JRadioButton genderFemale;
    private javax.swing.JRadioButton genderMale1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JPasswordField password;
    private javax.swing.JPasswordField passwordConfirm;
    private javax.swing.JLabel signUpbtn;
    private javax.swing.JLabel window_dragged;
    // End of variables declaration//GEN-END:variables
}
