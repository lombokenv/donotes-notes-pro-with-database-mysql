package Connection;

import java.sql.Connection;
import java.sql.DriverManager;

public class Koneksi {
    private String url = ("jdbc:mysql://localhost/dropnotesdb");
    private String user = "root";
    private String pass = "";
    Connection conn = null;
    
    public Connection connect(){
       
        try{
            conn = DriverManager.getConnection(url, user,pass);
            System.out.println("Suskes");
        } catch (Exception ex) {
           ex.printStackTrace();
        }
        return conn;
    }
}
