-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 15, 2019 at 05:32 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dropnotesdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id_categories` int(11) NOT NULL,
  `name_categories` varchar(30) NOT NULL,
  `relasi_categories` int(11) NOT NULL,
  `relasi_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id_categories`, `name_categories`, `relasi_categories`, `relasi_user`) VALUES
(35, 'web', 1, 1),
(36, 'programing', 1, 1),
(37, 'gaming', 1, 1),
(38, 'All', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `memiliki`
--

CREATE TABLE `memiliki` (
  `id_memiliki` int(11) NOT NULL,
  `relasi_user` int(11) NOT NULL,
  `relasi_notes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id_notes` int(11) NOT NULL,
  `title` varchar(35) NOT NULL,
  `isi_notes` text NOT NULL,
  `category` varchar(30) NOT NULL,
  `relasi_categories` int(11) DEFAULT NULL,
  `relasi_notes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id_notes`, `title`, `isi_notes`, `category`, `relasi_categories`, `relasi_notes`) VALUES
(24, 'Belajar Visual dengan Java', 'Tugas ini dibuat menggunakan bahasa java', 'programing', 1, 1),
(30, 'Apa itu Laravel', 'Laravel adalah kerangka kerja aplikasi web berbasis PHP yang open source, \nmenggunakan konsep model–view–controller. Laravel berada dibawah lisensi MIT, \ndengan menggunakan GitHub sebagai tempat berbagi kode', 'web', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(25) NOT NULL,
  `password` varchar(20) NOT NULL,
  `gender` varchar(12) NOT NULL,
  `relasi_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `name`, `email`, `password`, `gender`, `relasi_user`) VALUES
(24, 'romy', 'romy@gmail.com', '1', 'Male', 1),
(25, 'admin', 'admin@gmail.com', 'admin', 'Male', 2),
(26, 'admin1', 'admin1@gmail.com', 'admin1', 'Male', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id_categories`),
  ADD KEY `relasi_categories` (`relasi_categories`),
  ADD KEY `relasi_user` (`relasi_user`);

--
-- Indexes for table `memiliki`
--
ALTER TABLE `memiliki`
  ADD PRIMARY KEY (`id_memiliki`),
  ADD KEY `relasi_notes` (`relasi_notes`),
  ADD KEY `relasi_user` (`relasi_user`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id_notes`),
  ADD KEY `relasi_categories` (`relasi_categories`),
  ADD KEY `relasi_notes` (`relasi_notes`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `relasi_user` (`relasi_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id_categories` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `memiliki`
--
ALTER TABLE `memiliki`
  MODIFY `id_memiliki` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id_notes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`relasi_user`) REFERENCES `user` (`relasi_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `memiliki`
--
ALTER TABLE `memiliki`
  ADD CONSTRAINT `memiliki_ibfk_1` FOREIGN KEY (`relasi_user`) REFERENCES `user` (`relasi_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `memiliki_ibfk_2` FOREIGN KEY (`relasi_notes`) REFERENCES `notes` (`relasi_notes`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `notes_ibfk_1` FOREIGN KEY (`relasi_categories`) REFERENCES `categories` (`relasi_categories`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
